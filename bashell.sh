#!/bin/bash
#
# Copyright 2017 Chew, Kean Ho <kean.ho.chew@zoralab.com>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

################################################################################
# General Variables
################################################################################
export SOFTWARE_NAME="bashell"
export SOFTWARE_DESCRIPTION="a simple bash for unit-testing your BASH scripts."
export VERSION="1.4.2"
export CURRENT_DIRECTORY="$PWD"
export SCRIPT_DIRECTORY="$CURRENT_DIRECTORY/scripts"
export LOCAL_BASHELL_PATH="$SCRIPT_DIRECTORY/bashell.sh"
export TEST_DIRECTORY="$CURRENT_DIRECTORY/tests"
export TEST_SCRIPT_DIRECTORY="$TEST_DIRECTORY/scripts"
export TEST_TEMP_DIRECTORY="$TEST_DIRECTORY/temp"
export TEST_PASS=0
export TEST_SKIP=1
export TEST_FAIL=2

if [[ "${PS1-}" == "" || "${TERM-}" == "" || "${TERM-}" == "dumb" ]]; then
	export TERM="xterm-256color"
fi

tprint_horizontal_line() {
	if [[ "$1" != '' ]]; then
		printf '%*s\n' "${COLUMNS:-$(tput cols)}" '' | tr ' ' "$1"
	else
		printf '%*s\n' "${COLUMNS:-$(tput cols)}" '' | tr ' ' -
	fi
}
export -f tprint_horizontal_line

tprint() {
	echo "NOTE: $*"
	return 0
}
export -f tprint

tprint_test_description() {
	tprint_horizontal_line
	echo -e "TEST TITLE: $1" | fmt
	echo -e "TEST FOR: $2" | fmt
	echo -e "DESCRIPTION: $3" | fmt
	tprint_horizontal_line
}
export -f tprint_test_description

verdict() {
	case "$1" in
		-p|--passed)
			condition="PASSED"
			;;
		-f|--failed)
			condition="FAILED"
			;;
		-s|--skipped)
			condition="SKIPPED"
			;;
		*)
			condition="ERROR - do either '-p', '-f' or '-s'"
			;;
	esac
	shift 1
	message="$*"
	if [[ ${#message} != 0 ]]; then
		echo "[ $condition ] $message"
	else
		echo "[ $condition ]"
	fi
	unset message
}
export -f verdict


################################################################################
# ERROR_CODES
################################################################################
ERROR_ARGUMENT=1
ERROR_MISSING_FILE=2
ERROR_UNKNOWN_PATH=3
ERROR_ITEM_EXIST=4
ERROR_SYMLINK_PATH=5
ERROR_NOT_INSTALLED=6
ERROR_FILE_EXISTS=7
ERROR_INVALID_WORKSPACE=8

print_error_messages() {
	echo -n "error: "
	case "$1" in
		1|-a|--unknownArg)
			echo -n "Unknown arguments."
			;;
		-v|--unknownValue)
			echo -n "Unknown VALUE."
			;;
		3|-f|--unknownFilepath)
			echo -n "Unknown FILEPATH."
			;;
		-mTST|--missingTestScriptTemplate)
			echo -n "found TEST_SCRIPT_TEMPLATE_PATH declared but "
			echo -n "file not exist at $TEST_SCRIPT_TEMPLATE_PATH."
			;;
		6|-i|--notInstalled)
			echo -n "BaSHELL Framework not found. "
			;;
		7|-e|--fileExists)
			echo -n "File already exists!"
			;;
		8|-n|--invalidWorkspace)
			echo -n "Invalid Workspace. Are you in the workspace "
			echo -n "root directory?"
			;;
		*)
			echo -n "Unknown error."
			;;
	esac
	echo " Try 'bashell -h' for help."
}

################################################################################
# Codes
################################################################################
action="r"
action_type=""
filepath=""
suite=""
title=""
quiet_mode=false

process_filepath() {
	if [[ "$filepath" = "" ]]; then
		1>&2 echo "ERROR: missing FILEPATH."
		exit $ERROR_ARGUMENT
	fi
	suite=$(dirname $filepath)
	suite=${suite##./}
	suite=${suite##tests/scripts/}
	title=$(basename $filepath)
	title=${title%.test}
	filepath="${TEST_SCRIPT_DIRECTORY}/${suite}/${title}.test"
}

create_bash() {
	if [[ -f "$filepath" ]]; then
		print_error_messages --fileExists
		exit $ERROR_FILE_EXISTS
	fi

	echo '#!/bin/bash
################################
# User Variables               #
################################
VERSION="0.0.1"

################################
# App Variables                #
################################
action=""
message=""

################################
# Functions                    #
################################
print_version() {
	echo $VERSION
}

run() {
	if [[ "$message" == "" ]]; then
		echo "I see no message. Did you say something?"
	else
		echo "$message"
	fi
}

################################
# CLI Parameters and Help      #
################################
print_help() {
	echo "\
PROGRAM NAME
One liner description
-------------------------------------------------------------------------------
To use: $0 [ACTION] [ARGUMENTS]

ACTIONS
1. -h, --help			print help. Longest help is up
				to this length for terminal
				friendly printout.

2. -r, --run			run the program. In this case,
				says the message.
				COMPULSORY VALUES:
				1. -r \"[message you want]\"

				COMPULSORY ARGUMENTS:
				1. -

				OPTIONAL ARGUMENTS:
				1. -

3. -v, --version		print app version.
"
}

run_action() {
case "$action" in
"r")
	run
	;;
"h")
	print_help
	;;
"v")
	print_version
	;;
*)
	echo "[ ERROR ] - invalid command."
	return 1
	;;
esac
}

process_parameters() {
while [[ $# != 0 ]]; do
case "$1" in
-r|--run)
	if [[ "$2" != "" && "${2:0:1}" != "-" ]]; then
		message="${@:2}"
		shift 1
	fi
	action="r"
	;;
-h|--help)
	action="h"
	;;
-v|--version)
	action="v"
	;;
*)
	;;
esac
shift 1
done
}

main() {
	process_parameters $@
	run_action
	if [[ $? != 0 ]]; then
		exit 1
	fi
}

if [[ $BASHELL_TEST_ENVIRONMENT != true ]]; then
	main $@
fi' > "$filepath"
	chmod +x "$filepath"
}

create_test_script() {
	process_filepath

	if [[ -f "$filepath" ]]; then
		1>&2 echo "ERROR: test script exists."
		exit $ERROR_FILE_EXISTS
	fi

	mkdir -p "$(dirname "$filepath")"

	if [[ -f "$TEST_SCRIPT_TEMPLATE_PATH" ]]; then
		cat "$TEST_SCRIPT_TEMPLATE_PATH" > "$filepath"
	else
		echo "\
#!/bin/bash
TITLE='$title title here.'
SUITE='$suite'
DESCRIPTION='
Give a short description about your test script.
'
tprint_test_description \"\$TITLE\" \"\$SUITE\" \"\$DESCRIPTION\"
################################################################################
DEMO='pass'

if [[ \"\$DEMO\" == 'fail' ]]; then
	verdict --failed \"test failed message: \$DEMO\"
	exit \$TEST_FAIL
fi

if [[ \"\$DEMO\" == 'skip' ]]; then
	verdict --skipped \"skip test message: \$DEMO\"
	exit \$TEST_SKIP
fi

verdict --passed
exit \$TEST_PASS" > "$filepath"
	fi
	chmod +x "$filepath"
}

create_local_bashell() {
	if [[ "$0" != "" && ! -f "$LOCAL_BASHELL_PATH" ]]; then
		mkdir -p "$SCRIPT_DIRECTORY"
		cp -f "$0" "$LOCAL_BASHELL_PATH"
	fi
}

create_test_directory() {
	if [[ -d "$TEST_DIRECTORY" ]]; then
		1>&2 echo "ERROR: the test framework is already available."
		exit $ERROR_ITEM_EXIST
	fi

	mkdir -p "$TEST_SCRIPT_DIRECTORY"
	mkdir -p "$TEST_TEMP_DIRECTORY"
	touch "${TEST_SCRIPT_DIRECTORY}/.gitkeep"
	touch "${TEST_TEMP_DIRECTORY}/.gitkeep"
}

create_framework() {
	create_local_bashell
	create_test_directory
}

run_create() {
	case "$action_type" in
		"framework")
			create_framework
			;;
		"script")
			create_test_script
			;;
		"bash")
			create_bash
			;;
		*)
			print_error_messages --unknownValue
			exit $ERROR_ARGUMENT
			;;
	esac
}

delete_test_script() {
	process_filepath

	# delete target
	rm -rf "$filepath" &> /dev/null

	# delete suite if empty
	directory=$(dirname "$filepath")
	ret=$(ls "$directory")
	if [[ -d $directory && "$ret" == "" ]]; then
		rm -rf "$directory"
	fi
}

delete_local_bashell() {
	rm "$LOCAL_BASHELL_PATH" &> /dev/null
	if [[ "$(ls "$SCRIPT_DIRECTORY")" == "" ]]; then
		rm -rf "$SCRIPT_DIRECTORY"
	fi
}

delete_test_directory() {
	if [[ ! -d "$TEST_DIRECTORY" ]]; then
		exit 0
	fi

	# send warning
	1>&2 echo "\
WARNING: this will delete every scripts in the test directory. ARE YOU SURE?"
	select yn in "YES" "No"; do
		case "$yn" in
			YES)
				break
				;;
			No)
				exit 0
				;;
		esac
	done

	# delete framework
	rm -rf "$TEST_DIRECTORY" &> /dev/null
}

delete_framework() {
	delete_test_directory
	delete_local_bashell
}

run_delete() {
	case "$action_type" in
		"framework")
			delete_framework
			;;
		"script")
			delete_test_script
			;;
		*)
			print_error_messages --unknownValue
			exit $ERROR_ARGUMENT
			;;
	esac
}

run_single_test() {
	# prepare
	mkdir -p "$TEST_TEMP_DIRECTORY"

	# run
	printout="$(2>&1 bash "$1")"
	verdict=$?
	if [[ "$quiet_mode" == "false" || "$verdict" != "$TEST_PASS" ]]; then
		echo "$printout"
		echo -e "\n"
	fi

	# cleanup
	unset printout
	rm -rf "$TEST_TEMP_DIRECTORY"
	mkdir -p "$TEST_TEMP_DIRECTORY"
	touch "$TEST_TEMP_DIRECTORY/.gitkeep"

	return $verdict
}

run_multiple_tests() {
	# prepare
	while IFS=$'\n' read -r line; do list+=("$line"); done < \
		<(find "$1" -type f -name "*.test")
	total_count=${#list[@]}
	passed_count=0
	failed_count=0
	skipped_count=0

	# run
	echo "\
BaSHELL Test Framework
----------------------
Build for testing BASH scripts

Total test cases to run : $total_count
run quietly             : $quiet_mode"

	if [[ $quiet_mode == false ]]; then
		tprint_horizontal_line "="
	fi

	for test_script in "${list[@]}"; do
		run_single_test "$test_script"
		ret=$?
		if [ $ret == $TEST_SKIP ]; then
			((skipped_count++))
		elif [ $ret == $TEST_PASS ]; then
			((passed_count++))
		else
			((failed_count++))
		fi
	done
	unset ret

	tprint_horizontal_line "="
	echo "TEST RESULTS"
	echo "------------"
	echo "Total: $total_count"
	echo "Passed: $passed_count"
	echo "Failed: $failed_count"
	echo "Skipped: $skipped_count"
	tprint_horizontal_line "="

	# clean up
	unset BASHELL_TEST_ENVIRONMENT
	if [[ "$failed_count" != 0 ]]; then
		return 1
	fi
}

run_test() {
	if [[ ! -d "$TEST_SCRIPT_DIRECTORY" ]]; then
		print_error_messages --invalidWorkspace
		exit $ERROR_INVALID_WORKSPACE
	fi
	export BASHELL_TEST_ENVIRONMENT=true

	# if filepath is given. Run single test.
	if [[ -f "$filepath" ]]; then
		ext=$(basename "$filepath")
		ext="${ext##*.}"
		if [[ "$ext" != "test" ]]; then
			print_error_messages  --unknownFilepath
			exit $ERROR_UNKNOWN_PATH
		fi
		run_single_test "$filepath"
		verdict=$?
	elif [[ -d "$filepath" ]]; then
		run_multiple_tests "$filepath"
		verdict=$?
	else
		run_multiple_tests "$TEST_SCRIPT_DIRECTORY"
		verdict=$?
	fi
	unset BASHELL_TEST_ENVIRONMENT
	exit $verdict

}

print_help() {
	echo "\
$SOFTWARE_NAME - $SOFTWARE_DESCRIPTION
$0 [options] [value] [options] [value] ...

Available options:
-h, --help                        show help page.

-c, --create [ VALUE ]            create an object. VALUE can be:
    VALUE: framework              1) The BaSHELL framework.
    VALUE: script [FILEPATH]      2) A test script with FILEPATH.
                                     E.g.: 'directory/name' for
                                     'directory/name.test'.
                                     NOTE: make sure it doesn't
                                     exist otherwise, it will
                                     fail.
    VALUE: bash [FILEPATH]        3) Create a BaSHELL friendly
                                     unix argument bash script
                                     with the given FILEPATH
                                     and name.

-d, --delete [ VALUE ]            delete an object. VALUE can be:
    VALUE: framework              1) The BaSHELL framework.
    VALUE: script [FILEPATH]      2) A test script in FILEPATH.
                                     E.g.: 'directory/name' for
                                     'directory/name.test'.
                                     NOTE: if the test folder
                                     is empty, command will
                                     remove that folder.

-r, --run [ VALUE ]               execute all tests run.
    VALUE: <nothing>              1) execute all tests (default) when no
                                     VALUE is given.
    VALUE: [FILEPATH]             2) run a particular test from the given
                                     FILEPATH.
                                     e.g. -r ./tests/scripts/sample.test

-i, --install                     install BaSHELL framework.

-u, --uninstall                   uninstall BaSHELL Framework.

Optional options:
-q, --quiet                       run the test quietly, without
                                  printout except error occured.
"
	return 0
}

run_actions() {
	case "$action" in
	v)
		echo "$VERSION"
		;;
	h)
		print_help
		;;
	c)
		run_create
		;;
	d)
		run_delete
		;;
	i)
		run_install
		;;
	u)
		run_uninstall
		;;
	*)
		run_test
		;;
	esac
}

process_parameters() {
while [[ $# != 0 ]]; do
case "$1" in
	-v|--version)
		echo "$VERSION"
		exit 0
		;;
	-h|--help)
		action="h"
		shift 1
		;;
	-c|--create)
		action="c"
		shift 1

		# second args
		if [[ "$1" != "" && "${1:0:1}" != "-" ]]; then
			action_type="$1"
			shift 1
		fi

		# third args
		if [[ "$1" != "" && "${1:0:1}" != "-" &&
			("$action_type" == "script" ||
			 "$action_type" == "bash") ]]; then
			filepath="$1"
			shift 1
		fi
		;;

	-d|--delete)
		action="d"
		shift 1

		# second args
		if [[ "$1" != "" && "${1:0:1}" != "-" ]]; then
			action_type="$1"
			shift 1
		fi

		# third args
		if [[ "$1" != "" && "${1:0:1}" != "-" &&
			"$action_type" == "script" ]]; then
			filepath="$2"
			shift 1
		fi
		;;
	-r|--run)
		action="r"
		shift 1

		# second args
		if [[ "$1" != "" && "${1:0:1}" != "-" ]]; then
			filepath="$1"
			shift 1
		fi
		;;
	-i|--install)
		action="c"
		action_type="framework"
		shift 1
		;;
	-u|--uninstall)
		action="d"
		action_type="framework"
		shift 1
		;;
	-q|--quiet)
		quiet_mode=true
		shift 1
		;;
	*)
		shift 1
		;;
	esac
done
}

main() {
	process_parameters $@
	run_actions
}

if [[ $BASHELL_TEST_ENVIRONMENT != true ]]; then
	main $@
fi
