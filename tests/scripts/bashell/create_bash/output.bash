#!/bin/bash
################################
# User Variables               #
################################
VERSION="0.0.1"

################################
# App Variables                #
################################
action=""
message=""

################################
# Functions                    #
################################
print_version() {
	echo $VERSION
}

run() {
	if [[ "$message" == "" ]]; then
		echo "I see no message. Did you say something?"
	else
		echo "$message"
	fi
}

################################
# CLI Parameters and Help      #
################################
print_help() {
	echo "\
PROGRAM NAME
One liner description
-------------------------------------------------------------------------------
To use: $0 [ACTION] [ARGUMENTS]

ACTIONS
1. -h, --help			print help. Longest help is up
				to this length for terminal
				friendly printout.

2. -r, --run			run the program. In this case,
				says the message.
				COMPULSORY VALUES:
				1. -r \"[message you want]\"

				COMPULSORY ARGUMENTS:
				1. -

				OPTIONAL ARGUMENTS:
				1. -

3. -v, --version		print app version.
"
}

run_action() {
case "$action" in
"r")
	run
	;;
"h")
	print_help
	;;
"v")
	print_version
	;;
*)
	echo "[ ERROR ] - invalid command."
	return 1
	;;
esac
}

process_parameters() {
while [[ $# != 0 ]]; do
case "$1" in
-r|--run)
	if [[ "$2" != "" && "${2:0:1}" != "-" ]]; then
		message="${@:2}"
		shift 1
	fi
	action="r"
	;;
-h|--help)
	action="h"
	;;
-v|--version)
	action="v"
	;;
*)
	;;
esac
shift 1
done
}

main() {
	process_parameters $@
	run_action
	if [[ $? != 0 ]]; then
		exit 1
	fi
}

if [[ $BASHELL_TEST_ENVIRONMENT != true ]]; then
	main $@
fi
