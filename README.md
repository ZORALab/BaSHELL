[![language](https://img.shields.io/badge/core%20language-bash-1313c3.svg?style=flat)]()
[![license](https://img.shields.io/badge/License-Apache%20License,%20Version%202.0-cc9900.svg?style=flat)]()
[![release](https://img.shields.io/badge/release-gold%2C%20stable-yellow.svg?style=flat)]()

| Branch   | Test Status |
|:---------|:------------|
| `master` | [![pipeline status](https://gitlab.com/ZORALab/BaSHELL/badges/master/pipeline.svg)](https://gitlab.com/ZORALab/BaSHELL/commits/master) |
| `next`   | [![pipeline status](https://gitlab.com/ZORALab/BaSHELL/badges/next/pipeline.svg)](https://gitlab.com/ZORALab/BaSHELL/commits/next) |


[![banner](https://lh3.googleusercontent.com/kEzOKtj0OmVDfREYXFJ-TUWhTnQUYvb_aDEu_XOFLXNEvMb6Y4XKSsJxkAWtznYdCDvRSETocBb3m42dNicNdPjK_cQvFZp8OmyCWQmh4i1s5BRCawvrgg3z3cFdEZQOowvL7N1vXm3hEdVstBiRxbHOpLdz2ABbfTNOUj6PVixR9Zhs4ubc0Ku8jqnOrF9TY9e0yxi9SloV7xCW2efYrOgu0FWkXt1ysc6AWTPr-PTeqNjP5D5gbv3FZqwGwkKyVL6tNaagoDBZ56GGC0-jG3kju6t2Y7fA3jDJ6dVq0nOqm-AUtXrW9N4RH08sxAL4W6iwefNlretB0k8-YGFG_nRdGJXlh3lWLfBA6WIu4onro0joRCMIT2IObsXb_jUBqBiDGr-bK-R0UCI0niQwOH3IAQWRZWRY4rY_MaoQZWHR_DF6bCpT3KaEyPiJtpJwc5e2fmboMb9fl1BCBTNmIR-xk4OF8bkn6FIysMAu-SBYJzv7K33bkIAeB9DXp-Dc0IA-8i5V06urthdNAWU7f8OcrLQkdku5g1GnDUpB0qucyqSmC1wQwevfwaFGBhFS2FpOa_B1lFeu0_EBBsJYHFgo3z_GkmzAep9DVP8NnonefLwyBleM5wByxPYWXd63NBDwbOIMNgNYIdaCdKE0Fuxz7awCNbj2=w1782-h891-no)]()
# BaSHELL
A simple git repository framework for unit-testing your bash script. Itself is
a bash script too. Now, you can perform unit-testing and early development for
system-wide automation without downloading massive test tools or big footprint
dependencies.

> **Important**: BaSHELL uses `BASH`. Therefore, it's
> **not fully POSIX-Compliant**.

<br/>
*This project is sponsored by:*

[![ZORALab](https://lh3.googleusercontent.com/jwB6L7Jl2A9DkqolBhnR915KvgMid66zieSDqh5hMK6oMJR81mw_QWzynMEJL-R_8yA1yKXawvGy3wb3pFmfeoCu-mVrBFPhY6BaORathrfVogDNUvZ5WvKtpH9s7faaOMDP9_KKUBkWny87gnX1mNDclbJn1NqY6rF237CbYHSyqjnYaFtACRdqR7Pl4ZZeqzdaybQWsF2KWbyqSNfQA-wsMFGf9An_xxrvS-oUKqB7G_cxotfFqpK2NvVdhQ80G7scO3WHNqlhTKmcPrMCk-BlTyNCmLsaqeAw9rfWV66ty_ximBVMnOOHVOwW4hb3RCGG9X130FiaX9Vj4Retu4wSW8DpvrvBGJqfMqqYqNo4w2c0oQUiDn3sH0IKyNMuTGiegXZUt-IcLK437uwf_FGO42XBorQSB4bEt0ZoQweUVmOADM03VEivIKFaM8bI0obdBtFoOCSz29mUeF1Pa5Oc7EJPfGakC5TAf5HJsnbjYRvuVDsXshHuuZ7JH1JXs93SE_8mf-3flN3N18ORwvTIKsiGJ4t38pEk9QWqXt2PToBuCJBjBdAWFUwWEU1_NOIM8mY_rpfOH_AmH_CtX0Ql6KUF-sIcdQzKKSn4cQjICOvPfF5bSHOzXWzEwHeqvjFmkRybHH5HORdk-BaGRZQQh3so3h9m=w300-h100-no)](https://www.zoralab.com)



<br/><br/>
## Installing
### Snap
```bash
$ sudo snap install bashell
```

### Ubuntu
```bash
$ sudo add-apt-repository ppa:zoralab/bashell
$ sudo apt-get update -y
$ sudo apt install bashell
```

### Other Linux Systems
Use snap. Visit how to install at [Snapcraft.io](https://docs.snapcraft.io/core/install).


### Quick Conventional Way
```bash
$ sudo curl -o /usr/local/bin/bashell https://gitlab.com/ZORALab/BaSHELL/raw/master/bashell.sh && sudo chmod +x /usr/local/bin/bashell
```
> NOTE: To get update, just do the installation step above again. Curl will
> override the old version.


<br/>
## Backstory
This framework is quite simple: it uses a `tests` folder in your repository to
hold all the test scripts and test workspace. This allows you to run your test
environment solely within the repository.

The trade-off is however, requires you to have comply to BASH scripting best
practices, such as: `group execution as functions`, and using `main` function.
This way, it's easier to test your execution via calling the functions.

If you need a guide, we can offer our ZORALab best practice
[guidelines.](https://gitlab.com/ZORALab/Resources/blob/master/coding_guidelines/shell_bash.md)

> NOTE:
>
> It's not a rules or code of conducts. It's just our years of learning and
> compilation of the best from seeing and experiences.

BaSHELL, if installed locally into your git repository, occupies the following
file structure:
```
your-repository
    |-- ...
    |
	|-- tests
	|	|- scripts
	|	|- temp
	|
	|-- scripts
	    |- bashell.sh
```
a localized `bashell.sh` is available under the common `scripts` folder. This
is useful for continuous integration (CI) test machines that has limitation
with installing `bashell` packages.


<br/>
## Commands
At the very start, you might want to read the `help`. So in case of losing
yourself, always remember this command:
```bash
$ bashell -h
```


### Install a local BaSHELL script
At the very start in your repository, you can perform a local installation
command:
```bash
$ bashell -i
```


### Creating a bashell friendly bash script
To create a bash script that is friendly to BaSHELL, off the start:
```bash
$ bashell -c bash "path/to/the/file/with/filename"
```

> NOTE:
>
> If the file exists, the command will reject to override the file.


### Creating a test script
To create a test script, you can use the following command:
```bash
$ bashell -c script "test_suite/test_title"
```

> NOTE:
>
> this will create a `test_suite` folder containing `test_title.test` BASH
> script with a `.test` extensions.


### Creating a test script using custom template
To use a custom test script, you export the `$TEST_SCRIPT_TEMPLATE_PATH`
environment variable pointing to your template before creating:
```bash
$ export TEST_SCRIPT_TEMPLATE_PATH=/path/to/custom/script.sh
$ bashell -c script "test_suite/test_title"
$ unset TEST_SCRIPT_TEMPLATE_PATH                 # reset back to standard
```


### Run a specific test script
To run a single test script, you can use the following command:
```bash
$ ./scripts/bashell.sh -r tests/scripts/test_suite/test_title.test
```

> NOTE:
>
> if you run the quiet flag `-q`, this run will print nothing.

### Run all test scripts
To execute a full test run, at the root repository, you can just issue:
```bash
$ ./scripts/bashell.sh -r
```

To run quietly and only reports when failed/skipped cases occured, you can pass
in the `-q` quiet flag.
```bash
$ ./scripts/bashell.sh -r -q
```

### Run a suite of test scripts
To run all the test scripts in a specific suite/folder, you can just pass in
the folder path instead of the specific script:
```bash
$ ./scripts/bashell.sh -r tests/scripts/test_suite
```

To run quietly and only reports when failed/skipped cases occured, you can pass
in the `-q` quiet flag.
```bash
$ ./scripts/bashell.sh -r tests/scripts/test_suite -q
```

> NOTE:
>
> Pass in the path to the folder. **DO NOT** pass the wildcard (`*`) at the end
> of the path (e.g.: `tests/scripts/test_suite/*`) . It won't work and chaos can
> occurs.


### Version
To get your current BaSHELL version. Use `-v`:
```bash
$ ./scripts/bashell.sh -v
```


<br/>
## Issues? Bugs? New Idea? Smelly Code?
Please feel free to file a discussion
[here](https://gitlab.com/ZORALab/BaSHELL/issues) and label it as ~"Discussion"
label.


<br/>
## Creating your own bash script
As you create your own bash script (not the test script), we recommend your
script following this structure as an examples:
1. Have the executable listed out on the first line as a shebang line
(`#!/bin/bash`)
2. Specify your libraries into an array and loop through them for `sourcing`.
3. The 80 characters barrier line to split the actual code.
4. export or set any environment variables or shared variables appropriately.
5. have your codes packed into a list of functions, then execute them through a
`main` function.
6. a `main` function wraps up the script executions.
8. unset all variables after use.

A good example is to read up the `bashell.sh` script itself. This framework
is an inception that allows `bashell.sh` uses its ability to test itself.

> NOTE:
>
> test scripts will `source` your script and run testing against those
> functions. Fortunately and unfortunately, `source` will execute `main` code
> if it is unwrapped. Use:
>
> ```bash
> if [[ $BASHELL_TEST_ENVIRONMENT != true ]]; then
>	main $@
> fi
> ```


<br/>
## Writing the test script
### Header
Your test script comes with pre-configured test settings and printout, separated
by the barrier line (`####`). There are 4 basic information for each test case:
1. the BASH shebang line `#!/bin/bash`
2. `TITLE` = for test case title: a short description about this test case
3. `SUITE` = referring to test subject.
4. `DESCRIPTION` = the longer version about the test case. Keep in crisp and
simple to prevent bloating your test log.

`tprint_test_description` will handle all the printing so
**avoid deleting it at all cost**. An example would be:
```bash
#!/bin/bash
TITLE='execute_xcode_test() is working when 1 device is detected'
SUITE='demo_source_code/execute_xcode_test'
DESCRIPTION='
execute_xcode_test() should work properly without depending on device
detection.
'
tprint_test_description "$TITLE" "$SUITE" "$DESCRIPTION"
################################################################################
```


### Test Codes
Your test code is below the barrier line (`####`). Here, we divide the notes
into multiple sub-sections.

#### Dependencies
For core and common operations, you are **strongly** encouraged to use
[BusyBox](https://busybox.net/downloads/BusyBox.html) commands to operate your
test scripts. This is to ensure there is no extra unusual dependencies that
sends your other developers wondering what's going on.

If you do need to use one, ensure you have the installation packs together
before using it.

#### Environment Variables
BaSHELL prepared a list of environment variables for you to simplify the access.
Here's the map:
```
your-repository               $CURRENT_DIRECTORY
    |-- mycode.sh
    |-- ...
    |
	|-- tests                 $TEST_DIRECTORY
	|	|- scripts            $TEST_SCRIPT_DIRECTORY
	|	|- temp               $TEST_TEMP_DIRECTORY
	|
	|-- scripts               $SCRIPT_DIRECTORY
	    |- bashell.sh
	    |- styling.sh
```

1. `$CURRENT_DIRECTORY` is for you to get your subject source codes. The first
thing to do in the test codes is to `source` your subject. Example, based on
the map, it is:
```bash
source $CURRENT_DIRECTORY/mycode.sh
```

2. `$SCRIPT_DIRECTORY` is for you to run your custom scripts easily. Example,
based on the map, says we need to run `styling.sh` before anything else, we
write:
```bash
"$SCRIPT_DIRECTORY/styling.sh"
```

3. `$TEST_TEMP_DIRECTORY` is the test workspace for you to operate. This is
the temporary storage space for you to run your test. Example, say we want
to store the output of our `mycode.sh`, we do:
```bash
"./$CURRENT_DIRECTORY/mycode.sh" > "$TEST_TEMP_DIRECTORY/output"
```

4. `$TEST_SCRIPT_DIRECTORY` is for you to source any of your test artifact. Say
you want to `diff` the output of the `mycode.sh`, and you save the artifact
next to your source code, you can do something like:
```bash
diff "$TEST_SCRIPT_DIRECTORY/path/to/artifact" "$TEST_TEMP_DIRECTORY/output"
```

#### Concluding a Case
As you code your test cases and reaching a conclusion, BaSHELL provides the
conclusion functions and its return values. Use `exit` at all cost.
A proper verdict is usually comes with both the verdict and exit code.

This is the function for expressing your verdict:
```bash
verdict [conclusion_type] [your message]
```
> [conclusion_type] has:
> 1. `--failed` -- indicates it is a failed verdict
> 2. `--passed` -- indicates it is a passing verdict
> 3. `--skipped`-- indicates it is a skipping verdict
>
> [your message] is optional string messages.

These are the exit codes to conclude the test:
```bash
$TEST_FAIL
$TEST_PASS
$TEST_SKIP
```

> pass it to `exit` function for ending the test accordingly.

<br/>
The following will be the full example for each cases:
```bash
if [[ ... ]]; then
	verdict --failed "missing dump file for parsing."
	exit $TEST_FAIL
fi

if [[ ... ]]; then
	verdict --passed
	exit $TEST_PASS
fi

if [[ ... ]]; then
	verdict --skipped "Dependency is missing. Skipping this test then."
	exit $TEST_SKIP
fi
```

#### Stubbing an executable
Sometimes you might want to isolate a command or function from your test script,
which is commonly called stubbing. In BASH, you can stub it using the `function`
with the same name as the executable. Just setup your stub before calling the
execution. For example, say `xcodebuild` is a Mac-specific command but BaSHELL
testing covers both mac and linux, we can stub it by:
```bash
xcodebuild() {
	if [[ "$1" == "test" ]]; then
		for LAST in $@; do true; done
		if [[ "$LAST" == "platform=$TEST_PLATFORM,id=${UDID_ARRAY[0]}" ]]; then
			return 0
		fi
	fi
	return -1
}

# without altering the original command in your source code
xcodebuild clean --workspace "$PROJECT_NAME".workspace --scheme "$SCHEME_NAME"
```

You can also stub variable values as well, example setting the following:
```bash
TEST_TEMP_DIRECTORY=/some/where/else
```
will cause the test script operates in a different workspace.

#### Logging info
In situation where you wish to printout some information during a test run,
BaSHELL supplies `tprint` function. You can use it like:
```bash
tprint "Current test value is $sample_value"
```

#### A Full Example
So stacking all of them up yields the following example:
```bash
#!/bin/bash
TITLE='run_create_test_script() will not override existing script'
SUITE='bashell/run_create_test_script'
DESCRIPTION='
run_create_test_script() will not override existing test script and inform
user instead about an existing file.
'
tprint_test_description "$TITLE" "$SUITE" "$DESCRIPTION"
################################################################################
source "$CURRENT_DIRECTORY/bashell.sh"

# stub the workspace environment variable
TEST_SCRIPT_DIRECTORY="$TEST_TEMP_DIRECTORY/scripts"

# prepare test environment
mkdir -p "$TEST_SCRIPT_DIRECTORY"
suite="testing/secondary"
filename="vortex.test"
parameter="$suite/$filename"
actual_fullpath="$TEST_SCRIPT_DIRECTORY/$suite/$filename"

# stub script creation function
correct_keyword="this is a custom keyword"
wrong_keyword="this is a wrong custom keyword"
keyword="$correct_keyword"
create_test_script() {
  echo "$keyword" > "$actual_fullpath"
}

# test the function from the source codes
run_create_test_script "$parameter" &> /dev/null
if [[ ! -f "$actual_fullpath" ]]; then
  verdict --failed "file does not exist."
  exit $TEST_FAIL
fi

outcome=$(cat "$actual_fullpath" | grep "$correct_keyword")
if [[ "$outcome" == "" ]]; then
  verdict --failed "got corrupted file before overriding instead."
  exit $TEST_FAIL
fi

# override creation function
keyword="$wrong_keyword"
run_create_test_script "$parameter" &> /dev/null
ret=$?
if [[ $ret == 0 ]]; then
  verdict --failed "override happened."
  exit $TEST_FAIL
fi

outcome=$(cat "$actual_fullpath" | grep "$correct_keyword")
if [[ "$outcome" == "" ]]; then
  verdict --failed "got corrupted file after overriding prevention."
  exit $TEST_FAIL
fi

verdict --passed
exit $TEST_PASS
```

Want more example? Visit BaSHELL original test scripts:
[here](https://gitlab.com/ZORALab/BaSHELL/tree/master/tests/scripts)


<br/>
## Environment Variables
1. `CURRENT_DIRECTORY` - current root directory.
2. `TEST_DIRECTORY` - bashell tests directory, containing `script` and `temp`.
3. `TEST_SCRIPT_DIRECTORY` - bashell test scripts directory. the `script`.
4. `TEST_TEMP_DIRECTORY` - bashell temporary working directory. the `temp`.
5. `BASHELL_TEST_ENVIRONMENT` - bashell environment flag. Either true/false.


<br/>
## Error Codes
```bash
ERROR_ARGUMENT=1
ERROR_MISSING_FILE=2
ERROR_UNKNOWN_PATH=3
ERROR_ITEM_EXIST=4
ERROR_SYMLINK_PATH=5
ERROR_NOT_INSTALLED=6
ERROR_FILE_EXISTS=7
ERROR_INVALID_WORKSPACE=8
```

<br/>
## License
This repository is licensed under [Apache License, Version 2.0](https://gitlab.com/ZORALab/BaSHELL/blob/master/LICENSE).


<br/>
## Contribute
Before reading the guide, ensure you're fully abide to the following guidelines:
1. [BASH/Shell Script](https://gitlab.com/ZORALab/Resources/blob/master/coding_guidelines/shell_bash.md)
2. [YAML](https://gitlab.com/BaSHELL/Resources/blob/master/coding_guidelines/yml.md)


This [guide](https://gitlab.com/ZORALab/BaSHELL/blob/master/CONTRIBUTING.md)
should brings you up to speed for contributing back to this repository.

### Sponsor Us!
At ZORALab, we're committed to build more useful and practical open-source
software for free. However, To accelerate our efforts, this things doesn't come
cheap. =(

But you can help us by sponsoring us a small tip (can be a cup of coffee) through
our payment channel below! By sponsoring us, you'll get:
> 1. A customized `300px x 100px` badge designed and placed in the project
`sponsored` section!
> 2. Mentioned in the development and CHANGELOG!

Also, be sure to read the
[Terms and Conditions](https://gitlab.com/ZORALab/BaSHELL/blob/next/SPONSORSHIP.md).

> We know most people skips through reading the `Terms and Conditions` and
> went over our heads; Please hear us out by reading through it. We made it
> all our best making it easy for you to get throgh.

<br/>
To start, visit our payment gateway below and be sure to mention:
> 1. **MOST IMPORTANT** - Your `email` or `phone` contact. We'll communicate for
> designing your badge.
> 2. The `project` you're sponsoring.

[![Paypal](https://lh3.googleusercontent.com/8YAQpan47r2HKZn0BgdWQ2mfo0vlhrnQ0cKXbHrlt-R3PE3xeu4gO5KSVWWuFNILuMr9A5l9exMgNZ0EopBLXUocJh9F9fLZ4Q0USYR4QPax7RdAOnVl_K1fCIegPjB1thhfw2F43gd-d0gVPHURfKP1FZWKE_WMYLyyJV6UwCyE98CpjVqSzhRgCTCKTbilXfjJsrVVTLTCy8U9MA-m7NAYa67rvjZRkhVTPK7ncKZ1j4avNQxP4HdMR-F6sY3J-DQPsjNF3lXDxBxxLEdw0HrQQ2PLc4DhlHCa62N6dCs1zYNhoZPPGi8A36rPbynILwa1BK9CNeohlc1i5jVyIB6Fb1n-FiM_9L3yT3TYLcnQVH7M1LAqwsmDVgasE_wpG7-qDd5kVBaKAojkRcYbin93l2cl05M3VY0MKVkbE446D5cb_wJ-LJj6qL0az-Ut-u4cvsuT4Nnfl9E2GMt3mG2922B5kXJ-PadBIHB1YyGnhS8CAZJC5Te8TKZrdIld-tr2vOVhR0z2JB86dQUJ4vtsHeZAnazf_ad19gob-E5GJQPg2VDgwRfyvgMXDnMOKA-AJO4Tskohg9MPso4UN_vVsM3UwvVIc9mK1cB8BnrMBfm6Us-B6gT1XV4Teix1PgenhCNpZfFIK4vDtZKIuD8kSHE235w7=w200-h67-no)](https://www.paypal.me/zoralab/15)



**Thank you for helping us!**
