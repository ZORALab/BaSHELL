bashell (1.4.2DISTTAG) DISTSERIES; urgency=low

  *manta/manta.cfg: disable upstream for debian unstable
  *manta/DEBIAN/control: fix coreutils dependencies version at 8.21

 -- ZORALab Enterprise <tech@zoralab.com>  Mon, 01 Oct 2018 18:55:34 +0800

bashell (1.4.1DISTTAG) DISTSERIES; urgency=low

  *manta/SNAP/snapcraft.yaml: updated version to match 1.4.0

 -- ZORALab Enterprise <tech@zoralab.com>  Mon, 01 Oct 2018 17:46:33 +0800

bashell (1.3.1DISTTAG) DISTSERIES; urgency=low

  *bashell.sh: change fold command to fmt commands

 -- ZORALab Enterprise <tech@zoralab.com>  Fri, 27 Jul 2018 21:14:31 +0800

bashell (1.3.0DISTTAG) DISTSERIES; urgency=low

  *README.md: releasing the code quality as gold - stable  *SPONSORSHIP.md: added ZORALab Sponsorship mechanism into repository  *root: added SPONSORSHIP.md placeholder  *README.md: touch up readability and structure  *README.md: fixing typo and added logging info for writing test script  *README.md: added quiet flag and suite run instructions  *bashell.sh: added running suite feature to run_test()  *bashell.sh: added run test quietly feature  *README.md: updated to match the latest churn on bashell.sh  *root: refactored bashell.sh to comply BASH parameters concept  *manta: updated gpg key search using email  *bashell.sh: change local installation copy to use $0 variable  *Revert "bashell.sh: added local cache download and install"  *root: added manta packaging manager  *bashell.sh: updated bash script creator templates

 -- ZORALab Enterprise <tech@zoralab.com>  Fri, 27 Jul 2018 16:12:12 +0800

bashell (1.2.1DISTTAG) DISTSERIES; urgency=low

  *DEBIAN/dconfig: added Ubuntu 18.10 "Cosmic" series
  *SNAP/snapcraft.yaml: added plugs for bin/bashell command

 -- ZORALab Enterprise <tech@zoralab.com>  Wed, 27 Jun 2018 23:17:47 +0800

bashell (1.2.0DISTTAG) DISTSERIES; urgency=low

  *README.md: updated snap installation to have --classic flag
  *bashell.sh: implement strict test workspace cleaning
  *bashell.sh: added local cache download and install
  *test/scripts/bashell/create_bash: fix improper filepath for diff
  *bashell.sh: fix incorrect checking for $TERM
  *bashell.sh: fix run test exit always return 0 even in failed cases

 -- ZORALab Enterprise <tech@zoralab.com>  Fri, 25 May 2018 10:41:26 +0800

bashell (1.1.1DISTTAG) DISTSERIES; urgency=low

  *README.md: added Fedora installation guidelines using snaps
  *rpmbuild: added rpmbuild build system into repository

 -- ZORALab Enterprise <tech@zoralab.com>  Mon, 14 May 2018 07:53:42 +0800

bashell (1.1.0DISTTAG) DISTSERIES; urgency=low

  *SNAP: added ubuntu snaps packaging automation
  *bashell.sh: change bash template to use VERSION instead of version
  *README.md: updated documentation to relfect the bash creation feature
  *bashell.sh: added create bashell friendly linux bash script feature
  *bashell.sh: consolidate printouts under single echo

 -- ZORALab Enterprise <tech@zoralab.com>  Wed, 02 May 2018 22:10:44 +0800

bashell (1.0.3DISTTAG) DISTSERIES; urgency=low

  *DEBIAN/control: set debhelper to minimum version 9
  *DEBIAN: added debian .deb automation support
  *scripts/changelogger.bash: added auto changelog update script
  *bashell.sh: moved local script to scripts/bashell.sh
  *bashell.sh: added -v or --version feature
  *script/release.bash: added urgency field for release expectancy
  *scripts/release.bash: updated to use RFC 5322 date format
  *.gitignore: updated .gitignore file to the latest format
  *scripts/release.bash: added CHANGELOG.md auto-updater script
  * CHANGELOG.md: updated to latest format used by ZORALab release script
  * README.md: added BaSHELL new logo and banner

 -- ZORALab Enterprise <tech@zoralab.com>  Tue, 01 May 2018 18:39:18 +0800

bashell (1.0.2DISTTAG) DISTSERIES; urgency=low

  *updated README.md for installation and updating step.

 -- ZORALab Enterprise <tech@zoralab.com>  Fri, 30 Jun 2017 10:07:56 +0800

bashell (1.0.1DISTTAG) DISTSERIES; urgency=low

  *fixed missing .gitkeep file issue for bashell install.
  *fixed test script deleting original source code issue.

 -- ZORALab Enterprise <tech@zoralab.com>  Thu, 01 Jun 2017 09:22:56 +0800

bashell (1.0.0DISTTAG) DISTSERIES; urgency=low

  *created `bashell.sh` single script manager.
  *remove all contents except `tests/scripts` and `tests/temp`.
  *break version v0.1.0.
  *tested all `bashell.sh` functionalities.
  *added custom script template feature.
  *added install/uninstall framework.

 -- ZORALab Enterprise <tech@zoralab.com>  Sat, 11 Mar 2017 09:33:56 +0800
