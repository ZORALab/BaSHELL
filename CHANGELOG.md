# Version 1.4.2
## Mon, 01 Oct 2018 18:55:34 +0800 - (Urgency: low)
--------------------------------------------------------------------------------
1. 6d27d7b manta/manta.cfg: disable upstream for debian unstable
2. feed3cf manta/DEBIAN/control: fix coreutils dependencies version at 8.21

# Version 1.4.1
## Mon, 01 Oct 2018 17:46:33 +0800 - (Urgency: low)
--------------------------------------------------------------------------------
1. b36b26c manta/SNAP/snapcraft.yaml: updated version to match 1.4.0

# Version 1.3.1
## Fri, 27 Jul 2018 21:14:31 +0800 - (Urgency: low)
--------------------------------------------------------------------------------
1. 5a6eea2 bashell.sh: change fold comamnd to fmt commands

# Version 1.3.0
## Fri, 27 Jul 2018 16:12:12 +0800 - (Urgency: low)
--------------------------------------------------------------------------------
1. 10e8666 README.md: releasing the code quality as gold - stable
2. 8825f47 SPONSORSHIP.md: added ZORALab Sponsorship mechanism into repository
3. cbb85b6 root: added SPONSORSHIP.md placeholder
4. a2bed3b README.md: touch up readability and structure
5. 1ecd9ff README.md: fixing typo and added logging info for writing test script
6. cb7d33b README.md: added quiet flag and suite run instructions
7. 32471bf bashell.sh: added running suite feature to run_test()
8. a18f2c6 bashell.sh: added run test quietly feature
9. 1dedf57 README.md: updated to match the latest churn on bashell.sh
10. 0c99778 root: refactored bashell.sh to comply BASH parameters concept
11. 70def0b manta: updated gpg key search using email
12. 40caf56 bashell.sh: change local installation copy to use $0 variable
13. 5888b48 Revert "bashell.sh: added local cache download and install"
14. c2dda73 root: added manta packaging manager
15. abc28ed bashell.sh: updated bash script creator templates

# Version 1.2.1
## Wed, 27 Jun 2018 23:17:47 +0800 - (Urgency: low)
--------------------------------------------------------------------------------
1. 1dd3a9e DEBIAN/dconfig: added Ubuntu 18.10 "Cosmic" series
2. 2d7bcf0 SNAP/snapcraft.yaml: added plugs for bin/bashell command

# Version 1.2.0
## Fri, 25 May 2018 10:41:26 +0800 - (Urgency: low)
--------------------------------------------------------------------------------
1. 1940bee README.md: updated snap installation to have --classic flag
2. 0db9f61 bashell.sh: implement strict test workspace cleaning
3. 9bfff99 bashell.sh: added local cache download and install
4. 9d31614 test/scripts/bashell/create_bash: fix improper filepath for diff
5. 1f7a91c bashell.sh: fix incorrect checking for $TERM
6. e061bf6 bashell.sh: fix run test exit always return 0 even in failed cases

# Version 1.1.1
## Mon, 14 May 2018 07:53:42 +0800 - (Urgency: low)
--------------------------------------------------------------------------------
1. cd3f553 README.md: added Fedora installation guidelines using snaps
2. 5afe5d8 rpmbuild: added rpmbuild build system into repository

# Version 1.1.0
## Wed, 02 May 2018 22:10:44 +0800 - (Urgency: low)
--------------------------------------------------------------------------------
1. 026a458 SNAP: added ubuntu snaps packaging automation
2. 21798dc bashell.sh: change bash template to use VERSION instead of version
3. 8dcefe4 README.md: updated documentation to relfect the bash creation feature
4. 3c21fc9 bashell.sh: added create bashell friendly linux bash script feature
5. 8102e7c bashell.sh: consolidate printouts under single echo

# Version 1.0.3
## Tue, 01 May 2018 18:39:18 +0800 - (Urgency: low)
--------------------------------------------------------------------------------
1. 52928cd DEBIAN/control: set debhelper to minimum version 9
2. d698004 DEBIAN: added debian .deb automation support
3. 0b06de0 scripts/changelogger.bash: added auto changelog update script
4. 5af5d56 bashell.sh: moved local script to scripts/bashell.sh
5. 0b98869 bashell.sh: added -v or --version feature
6. 2471af3 script/release.bash: added urgency field for release expectancy
7. 7e8147b scripts/release.bash: updated to use RFC 5322 date format
8. 42ce243 .gitignore: updated .gitignore file to the latest format
9. 214ef71 scripts/release.bash: added CHANGELOG.md auto-updater script
10. e775c88 CHANGELOG.md: updated to latest format used by ZORALab release script
11. 8e895a8 README.md: added BaSHELL new logo and banner

# Version 1.0.2
## Fri, 30 Jun 2017 10:07:56 +0800 - (Urgency: low)
-------------------------------------------------------------------------------
1.         updated README.md for installation and updating step.

# Version 1.0.1
## Thu, 01 Jun 2017 09:22:56 +0800 - (Urgency: low)
-------------------------------------------------------------------------------
1.         fixed missing .gitkeep file issue for bashell install.
2.         fixed test script deleting original source code issue.

# Version 1.0.0
## Sat, 11 Mar 2017 09:33:56 +0800 - (Urgency: low)
-------------------------------------------------------------------------------
1.         created `bashell.sh` single script manager.
2.         remove all contents except `tests/scripts` and `tests/temp`.
3.         break version v0.1.0.
4.         tested all `bashell.sh` functionalities.
5.         added custom script template feature.
6.         added install/uninstall framework.

# Version 0.1.0
## Wed, 08 Mar 2017 04:28:56 +0800 - (Urgency: low)
-------------------------------------------------------------------------------
1.         added frameworks into repository.
